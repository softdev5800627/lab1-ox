/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.pattarapol.lab1ox;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Lab1OX {
    
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;
    private static String isContinue;

    public static void main(String[] args) {
        printWelcome();
        while(true){
        printTable();
        printTurn();
        inputRowCol();
            if(isWin()){
                printTable();
                printWin();
                break;
             
            } else if(isDraw()){
                printTable();
                printDraw();
                break;
            }
         switchPlayer(); 
        }
        inputContinue();
        
    }

    private static void printWelcome() {
        System.out.println("Welcome To OX Game");
        System.out.println("—-----------------");
    }

    private static void printTable() {
        for(int i = 0 ; i <3 ; i++){
            System.out.print("|");
            for(int j =0 ; j <3 ; j++){
                System.out.print(table[i][j]+"|");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.print(currentPlayer + " turn.");
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.print("Please input row[1-3] and column[1-3]: ");
        row = kb.nextInt()-1;
        col = kb.nextInt()-1;
         if(row >=0 && row <3 && col >= 0 &&col < 3 &&table[row][col]== '-'  ){  
             table[row][col] = currentPlayer;  
             return;
        }
        }
        
    }

    private static void switchPlayer() {
        if(currentPlayer == 'X'){
            currentPlayer = 'O';
        }else{
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if(checkRow()|| checkCol()|| checkX1() || checkX2()){
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println("Congratulations! Player " + currentPlayer +" wins!");
    }

    private static boolean checkRow() {
        for(int i = 0 ; i <3 ; i++){
                if(table[row][i] != currentPlayer)
                    return false;
                    
        }
            return true;
        }
    


    private static boolean checkCol() {
        for(int i = 0 ; i <3 ; i++){
                if(table[i][col] != currentPlayer)
                    return false;
                    
        }
            return true;
        
    }



    private static boolean checkX1() {
        if(table[0][0] == currentPlayer&&table[1][1] == currentPlayer&&table[2][2] == currentPlayer){
            return true;     
        }
 
         return false;

        

    }

    private static boolean checkX2() {
        if(table[0][2] == currentPlayer&&table[1][1] == currentPlayer&&table[2][0] == currentPlayer){
            return true;     
        }
        return false;     
    }

    private static boolean isDraw() {
        for(int i = 0 ; i < 3; i++ ){
            for(int j =0 ; j <3 ; j++){
                if(table[i][j] == '-'){
                    return  false;
                    
                }
            }
        }
        return true;
    }

    private static void printDraw() {
          System.out.println("The game is a draw!");
    }

    private static void inputContinue() {
        Scanner kb = new Scanner(System.in);
       while (true) {
           System.out.print("Do you want to play again? (y/n) :");
           isContinue = kb.next();
           if(isContinue.equalsIgnoreCase("y")){
               resetGame();
               main(null);
               break;
              } else if (isContinue.equalsIgnoreCase("n")) {
                System.out.println("Thank you for playing! Goodbye!");
                break;
            } else {
                System.out.println("Invalid input. Please enter 'y' or 'n'.");
            }
        }
    }

     private static void resetGame() {
        table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        currentPlayer = 'X';
    }

}



